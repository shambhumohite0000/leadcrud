package com.lead.service;

import com.lead.entity.LeadEntity;
import com.lead.repository.LeadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import org.springframework.beans.factory.annotation.Autowired(required=true);

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    public LeadEntity saveLead(LeadEntity lead) {

//        LeadEntity savedLeadObj = leadRepository.save(lead);
//
//        return savedLeadObj;

        return leadRepository.save(lead);

    }

}
