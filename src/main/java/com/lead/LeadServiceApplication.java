package com.lead;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class LeadServiceApplication {

	public static void main(String[] args) {
		System.out.println("Balya");
		SpringApplication.run(LeadServiceApplication.class, args);
	}

}


