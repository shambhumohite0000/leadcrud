package com.lead.controller;

import com.lead.entity.LeadEntity;
import com.lead.service.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
public class LeadController {

    @Autowired
    public LeadService leadService;

    @PostMapping("/lead")
    public ResponseEntity<String> saveLead(@RequestBody LeadEntity leadEntity)
    {
        leadService.saveLead(leadEntity);

        return new ResponseEntity<>(
                "Created Leads Successfully",
                HttpStatus.BAD_REQUEST);

    }

}
