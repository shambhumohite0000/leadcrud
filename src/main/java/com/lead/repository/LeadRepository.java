package com.lead.repository;

import com.lead.entity.LeadEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

//@Repository
//public interface LeadRepository extends JpaRepository<LeadEntity, String> {
//
//}

@Repository
public interface LeadRepository extends MongoRepository<LeadEntity, String> {

}
