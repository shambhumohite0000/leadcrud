package com.lead.entity;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import org.springframework.data.mongodb.core.mapping.Document;

@Entity
//@Table(name="LeadEntity")
@Document(collection="LeadEntity")
public class LeadEntity {

    @Column(name = "leadId", unique = true)
    @NotNull
    String leadId;

    @NotNull
    String firstName;

    String middleName;

    @NotNull
    String lastName;

    @NotNull
    String mobileNumber;

    @NotNull
    String Gender;

    @NotNull
    String mail;

    public LeadEntity() {
    }

    public LeadEntity(String leadId, String firstName, String middleName, String lastName, String mobileNumber, String gender, String mail) {
        this.leadId = leadId;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.mobileNumber = mobileNumber;
        Gender = gender;
        this.mail = mail;
    }

    public String getLeadId() {
        return leadId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getGender() {
        return Gender;
    }

    public String getMail() {
        return mail;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
